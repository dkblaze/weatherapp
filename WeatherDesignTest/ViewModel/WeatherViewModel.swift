
import Combine
import Foundation
class WeatherViewModel: ObservableObject {
    @Published private var data: Weather?

    @Published private var location = LocationManager()
    private let network = WeatherNetworking()
    private var cancellable: AnyCancellable?

    func fetchData() {
        DispatchQueue.main.async {
            self.cancellable = self.network.getRequest(latitude: "\(self.location.lastLocation?.coordinate.latitude ?? 0)", longitude: "\(self.location.lastLocation?.coordinate.longitude ?? 0)")
                .sink(receiveCompletion: { _ in }, receiveValue: { container in
                    self.data = container
                    print(self.data!)
                })
        }
    }

    var latitude: String {
        return String(format: "%.2f", data?.lat ?? 0.00)
    }

    var longitude: String {
        return String(format: "%.2f", data?.lon ?? 0.00)
    }

    var temperature: String {
        return String(format: "%.2f°C", data?.current.temp ?? 0.00)
    }

    var getMain: Int {
        return data?.current.weather.count ?? [Main]().count
    }

    var getInfo: String {
        var info = String()
        for value in 0 ..< getMain {
            info += String(data?.current.weather[value].main ?? "")
        }
        return info
    }

    var getNumberOfDays: Int {
        return data?.daily.count ?? [DailyWeather]().count
    }

    var getForecastTemp: [String] {
        var info = [String]()
        for value in 0 ..< getNumberOfDays {
            info.append(String(data?.daily[value].temp.day ?? 0.0))
        }
        return info
    }

    var feelsLike: String {
        String(format: "%.2f °C", data?.current.feelsLike ?? 0.00)
    }

    var humidity: String {
        return String(data?.current.humidity ?? 0)
    }

    var uvi: String {
        String(data?.current.uvi ?? 0.00)
    }

    var visibility: String {
        return String(data?.current.visibility ?? 0)
    }

    var windSpeed: String {
        return String(data?.current.windSpeed ?? 0.00)
    }

    var pressure: String {
        return String(data?.current.pressure ?? 0)
    }

    var condition: [Main] {
        return data?.current.weather ?? [Main]()
    }
}
