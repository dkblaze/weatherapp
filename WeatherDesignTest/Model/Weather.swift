
import Combine
import Foundation

struct Weather: Codable, Hashable {
    let lat: Double
    let lon: Double
    let current: Current
    let daily: [DailyWeather]
}

struct Current: Codable, Hashable {
    let temp: Double //
    let feelsLike: Double //
    let humidity: Int //
    let uvi: Double //
    let visibility: Int //
    let windSpeed: Double //
    let pressure: Int //
    let weather: [Main]

    enum CodingKeys: String, CodingKey {
        case temp, humidity, visibility, pressure, weather, uvi
        case feelsLike = "feels_like"
        case windSpeed = "wind_speed"
    }
}

struct Main: Codable, Hashable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

struct DailyWeather: Codable, Hashable {
    var temp: DailyTemp
}

struct DailyTemp: Codable, Hashable {
    var day: Double
}

enum ApiError: Error {
    case decodingError
    case httpError(Int)
    case unknown
}
