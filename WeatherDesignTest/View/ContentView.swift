

import SwiftUI

struct ContentView: View {
    @ObservedObject var test = WeatherViewModel()
    @State private var isPressed: Bool = false
    var body: some View {
        ZStack {
            WeatherCardView(temp: test.temperature, feelsLike: test.feelsLike, humidity: test.humidity, uvi: test.uvi, visibility: test.visibility, windSpeed: test.windSpeed, pressure: test.pressure, main: test.getInfo)
            Button(action: {
                self.test.fetchData()
            }, label: {
                Image(systemName: "arrow.clockwise.circle")
                    .font(.custom("Gill Sans", size: 40))
                    .foregroundColor(Color.black)

                    .padding(.leading, UIScreen.main.bounds.size.height / 3 + 30)
                    .padding(.bottom, UIScreen.main.bounds.size.height - 120)

            })
            Button(action: {
                self.isPressed.toggle()
            }, label: {
                Image(systemName: "arrowshape.turn.up.right.circle")
                    .font(.custom("Gill Sans", size: 40))
                    .foregroundColor(Color.black)

                    .padding(.trailing, UIScreen.main.bounds.size.height / 3 + 30)
                    .padding(.bottom, UIScreen.main.bounds.size.height - 120)

            })

                .sheet(isPresented: $isPressed) {
                    WeatherCardListView(content: test.getForecastTemp)
                }

        }.ignoresSafeArea(.all)
            .onAppear {
                self.test.fetchData()
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
