

import SwiftUI

struct WeatherCardView: View {
    var temp: String
    var feelsLike: String
    var humidity: String
    var uvi: String
    var visibility: String
    var windSpeed: String
    var pressure: String
    var main: String

    @State private var isAnimated = false
    var body: some View {
        ScrollView(.vertical) {
            VStack(alignment: .center) {
                ZStack(alignment: .center) {
                    Image("test2")
                        .resizable()
                        .frame(height: 400)
                        .cornerRadius(40)
                        .multilineTextAlignment(.center)

                    getSinWave(interval: UIScreen.main.bounds.width, amplitude: 100, baseline: -150 + UIScreen.main.bounds.height / 2)
                        .foregroundColor(Color("Background").opacity(0.4))
                        .offset(x: isAnimated ? -1 * UIScreen.main.bounds.width : 0)
                        .animation(
                            Animation.linear(duration: 2)
                                .repeatForever(autoreverses: false)
                        )
                    getSinWave(interval: UIScreen.main.bounds.width * 1.2, amplitude: 150, baseline: UIScreen.main.bounds.height / 2)
                        .foregroundColor(Color("Orange").opacity(0.4))
                        .offset(x: isAnimated ? -1 * (UIScreen.main.bounds.width * 1.2) : 0)
                        .animation(
                            Animation.linear(duration: 5)
                                .repeatForever(autoreverses: false)
                        )

                    VStack(alignment: .center) {
                        Text(main)
                            .font(.custom("Gill Sans", size: 25))
                            .fontWeight(.bold)

                        Text(temp)
                            .font(.custom("Gill Sans", size: 70))
                            .fontWeight(.bold)

                        Text("(Feels Like: \(feelsLike)")
                            .font(.custom("Gill Sans", size: 20))
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .padding(.bottom, 140)
                    }
                }.onAppear {
                    self.isAnimated = true
                }
                .padding(.bottom)

                HStack(alignment: .center, spacing: 20) {
                    ImageCard(imageId: "02d", title: "Condition")
                    InfoCard(imageId: "02d", title: "Humidity", content: humidity + "%")

                }.padding(.bottom, 5)
                HStack(alignment: .center, spacing: 20) {
                    InfoCard(imageId: "02d", title: "Wind Speed", content: windSpeed + "m/s")
                    InfoCard(imageId: "02d", title: "UV Index", content: uvi)
                }.padding(5)
                HStack(alignment: .center, spacing: 20) {
                    InfoCard(imageId: "02d", title: "Visibility", content: visibility + "m")
                    InfoCard(imageId: "02d", title: "Pressure", content: pressure + "hPa")
                }
            }
        }
        .background(Color("Background"))
        .ignoresSafeArea(.all)
    }
}
