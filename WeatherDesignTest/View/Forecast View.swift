
import SwiftUI

struct ForecastView: View {
    var title: String
    var content: String

    var body: some View {
        HStack(alignment: .center, spacing: 20) {
            Text(title)
                .font(.custom("Gill Sans", size: 20))
                .fontWeight(.semibold)
                .frame(width: 160)

            Text(content)
                .font(.custom("Gill Sans", size: 22))
                .fontWeight(.bold)
                .foregroundColor(.white)
                .padding()
                .frame(width: 180)
        }
        .frame(width: UIScreen.main.bounds.width - 50, height: 60)
        .background(Image("test2"))
        .cornerRadius(30)
        .shadow(color: Color.white.opacity(0.7), radius: 10, x: 0, y: 8)
        .padding(3)
    }
}
