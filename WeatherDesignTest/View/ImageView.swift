
import SwiftUI

struct ImageLoadingView: View {
    @ObservedObject var imageLoader: ImageLoader
    init(imageIcon: String) {
        imageLoader = ImageLoader(url: URL(string: "https://openweathermap.org/img/wn/\(imageIcon)@4x.png")!)
    }

    var body: some View {
        VStack {
            if imageLoader.image != nil {
                Image(uiImage: imageLoader.image!)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100)
            }
        }
        .onAppear(perform: imageLoader.load)
    }
}
