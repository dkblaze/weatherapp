

import SwiftUI

struct ImageCard: View {
    var imageId: String
    var title: String
    var body: some View {
        VStack(alignment: .center, spacing: 20) {
            Text(title)
                .font(.custom("Gill Sans", size: 20))
                .fontWeight(.semibold)
                .frame(width: 160)

            ImageLoadingView(imageIcon: imageId)
                .frame(width: 100, height: 60)
        }
        .frame(width: 160, height: 140)
        .background(Image("test2"))
        .cornerRadius(30)
        .shadow(color: Color.white.opacity(0.7), radius: 10, x: 0, y: 8)
    }
}
