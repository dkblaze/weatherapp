

import SwiftUI

struct WeatherCardListView: View {
    @State private var currentDate: String = Date().getFormattedDate()
    @State private var isAnimated: Bool = false
    var content: [String]

    var body: some View {
        ScrollView(.vertical) {
            VStack {
                ZStack(alignment: .center) {
                    Image("test2")
                        .resizable()
                        .frame(height: 150)
                        .cornerRadius(40)
                        .multilineTextAlignment(.center)

                    Text("Daily Forecast")
                        .font(.custom("Gill Sans", size: 35))
                        .fontWeight(.bold)
                    getSinWave(interval: UIScreen.main.bounds.width, amplitude: 100, baseline: -350 + UIScreen.main.bounds.height / 2)
                        .foregroundColor(Color("Background").opacity(0.4))
                        .offset(x: isAnimated ? -1 * UIScreen.main.bounds.width : 0)
                        .animation(
                            Animation.linear(duration: 2)
                                .repeatForever(autoreverses: false)
                        )
                    getSinWave(interval: UIScreen.main.bounds.width * 1.2, amplitude: 150, baseline: UIScreen.main.bounds.height / 2 + 100)
                        .foregroundColor(Color("Orange").opacity(0.4))
                        .offset(x: isAnimated ? -1 * (UIScreen.main.bounds.width * 1.2) : 0)
                        .animation(
                            Animation.linear(duration: 5)
                                .repeatForever(autoreverses: false)
                        )

                }.onAppear {
                    self.isAnimated = true
                }
                .padding()
                ForecastView(title: currentDate.convertToNextDate(days: 1), content: content[1])
                ForecastView(title: currentDate.convertToNextDate(days: 2), content: content[2])
                ForecastView(title: currentDate.convertToNextDate(days: 3), content: content[3])
                ForecastView(title: currentDate.convertToNextDate(days: 4), content: content[4])
                ForecastView(title: currentDate.convertToNextDate(days: 5), content: content[5])
                ForecastView(title: currentDate.convertToNextDate(days: 6), content: content[6])
                ForecastView(title: currentDate.convertToNextDate(days: 7), content: content[7])
            }
        }.ignoresSafeArea(.all)
    }
}
