
import Combine
import Foundation

class WeatherNetworking {
    private let key = "6fa86c2f0833095bf7eb04090edfedc8"

    func getRequest(latitude: String, longitude: String) -> AnyPublisher<Weather, ApiError> {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/onecall?lat=\(latitude)&lon=\(longitude)&units=metric&appid=6fa86c2f0833095bf7eb04090edfedc8") else {
            preconditionFailure("Invalid URL")
        }
        let request = URLRequest(url: url)

        return URLSession.shared
            .dataTaskPublisher(for: request)
            .receive(on: DispatchQueue.main)
            .mapError { _ in ApiError.unknown }
            .flatMap { data, response -> AnyPublisher<Weather, ApiError> in
                if let response = response as? HTTPURLResponse {
                    if (200 ... 299).contains(response.statusCode) {
                        return Just(data)
                            .decode(type: Weather.self, decoder: JSONDecoder())
                            .mapError { _ in .decodingError }
                            .eraseToAnyPublisher()
                    } else {
                        return Fail(error: ApiError.httpError(response.statusCode))
                            .eraseToAnyPublisher()
                    }
                }
                return Fail(error: ApiError.unknown)
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}
