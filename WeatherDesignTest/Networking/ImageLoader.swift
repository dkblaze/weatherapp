

import Combine
import Foundation
import SwiftUI

enum ImageLoadingError: Error {
    case incorrectData
}

final class ImageLoader: ObservableObject {
    @Published private(set) var image: UIImage? = nil

    private let url: URL
    private var cancellable: AnyCancellable?

    init(url: URL) {
        self.url = url
    }

    func load() {
        cancellable = URLSession.shared
            .dataTaskPublisher(for: url)
            .tryMap { data, _ in
                guard let image = UIImage(data: data) else {
                    throw ImageLoadingError.incorrectData
                }
                return image
            }
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { image in
                    self.image = image
                }
            )
    }
}
