//
//  WeatherDesignTestApp.swift
//  WeatherDesignTest
//
//  Created by Chris on 9/8/20.
//

import SwiftUI

@main
struct WeatherDesignTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
